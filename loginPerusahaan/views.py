from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from .models import User
from forumLowongan.models import Forum, Comment
# Create your views here.
response={}
def index(request):
    response['author'] = "Rai Indhira" #TODO Implement yourname
    html = 'daftar_login.html'

    readData = User.objects.all()
    readForum = Forum.objects.all()

    response['dataUser'] = readData
    response['forumLowongan'] = readForum
    print(User.objects.all().values_list('name', flat=True) )
    return render(request, html, response)

def add_session(request):
    if request.method == 'POST':
        name = request.POST['name']
        id = request.POST['id']
        request.session['user_login'] = id
        request.session['name'] = name
        try:
            user = User.objects.get(id = id)
        except Exception as e:
            user = User()
            user.id = id
            user.name = name
            user.save()
        return HttpResponseRedirect(reverse('login-perusahaan:index'))

def remove_session(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('fitur-login:index'))

