from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.

class ProfileUnitTest(TestCase):
    def test_profile_is_exist(self):
        response = Client().get('/profile-perusahaan/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/profile-perusahaan/')
        self.assertEqual(found.func, index)