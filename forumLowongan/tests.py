from django.test import TestCase, Client
from django.urls import resolve
from .views import index, paginate_page
from .models import Forum
from unittest.mock import patch
from django.db.models.manager import Manager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your tests here.
class ForumTest(TestCase):
    def test_status_using_index_func(self):
        found = resolve('/forum-lowongan/')
        self.assertEqual(found.func, index)

    def test_model_can_create(self):
        Forum.objects.create(content="I'm currently happy")

        counter_all_available_activity = Forum.objects.all().count()
        self.assertEqual(counter_all_available_activity, 1)

    def test_add_forum_reject_invalid_POST(self):
        response = self.client.post('/forum-lowongan/add/', data={'content': ''})
        html_response = response.content.decode('utf8')
        self.assertNotEqual(response.status_code, 400)

    def test_add_forum_GET_redirects(self):
        response = self.client.get('/forum-lowongan/add/')
        self.assertEqual(response.status_code, 302)

    def test_invalid_page_pagination_number(self):
        data = ["asik", "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe", "seru", "asik",
                "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe",
                "seru", "asik", "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe", "seru"]
        test1 = paginate_page("...", data)
        test2 = paginate_page(-1, data)
        with patch.object(Manager, 'get_or_create') as a:
            a.side_effect = PageNotAnInteger("page number is not an integer")
            res = paginate_page("...", data)
            self.assertEqual(res['page_range'], test1['page_range'])
        with patch.object(Manager, 'get_or_create') as a:
            a.side_effect = EmptyPage("page number is less than 1")
            res = paginate_page(-1, data)
            self.assertEqual(res['page_range'], test2['page_range'])
