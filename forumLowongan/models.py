from django.db import models

# Create your models here.
class Forum(models.Model):
	title = models.CharField(max_length=140)
	date = models.DateTimeField(auto_now_add=True)
	# company = models.ForeignKey(Company)
	content = models.TextField(max_length=500)
	komentar = []

class Comment(models.Model):
    text = models.CharField(max_length=140)
    content = models.ForeignKey(Forum)
    created_on = models.DateTimeField(auto_now_add=True)