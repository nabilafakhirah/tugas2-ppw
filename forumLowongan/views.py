from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from .models import Forum, Comment
from .forms import ForumForm
from loginPerusahaan.views import index
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
response = {}
def index(request):
    if 'user_login' in request.session:
        forum_list = Forum.objects.all()
        #Paginator
        page = request.GET.get('page',1)
        paginate_data = paginate_page(page, forum_list)
        forum = paginate_data['data']
        page_range = paginate_data['page_range']

        response = {"forum_list": forum, "page_range": page_range}
        return render(request, 'forum.html', response)
    else:
        return HttpResponseRedirect(reverse('login-perusahaan:index'))

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 5 else 0
    end_index = 5 if index < max_index - 5 else max_index
    # Get our new page range. In the latest versions of Django page_range returns
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data
    

def add(request):
    if request.method == 'POST':
        form = ForumForm(request.POST)
        response['form'] = form
        if form.is_valid():
            Forum.objects.create(content=form.cleaned_data['content'], title=form.cleaned_data['title'])
            forum = Forum.objects.all()
            response['forum'] = forum
            return redirect('/forum-lowongan/')
        else:
            forum = Forum.objects.all()
            response['forum'] = forum
            response['form'] = form
            return render(request, 'daftar_login.html', response)
    return redirect('/forum-lowongan/')

def delete_forum(request, id):
    to_delete = Forum.objects.get(id=id)
    to_delete.delete()
    return redirect('/forum-lowongan/')