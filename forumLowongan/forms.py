from django import forms

class ForumForm(forms.Form):
    title = forms.CharField(max_length=140)
    content = forms.CharField(max_length=500)