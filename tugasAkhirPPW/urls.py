"""tugasAkhirPPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
import loginPerusahaan.urls as loginPerusahaan
import tanggapanLowongan.urls as tanggapanLowongan
import forumLowongan.urls as forumLowongan
import profilPerusahaan.urls as profilPerusahaan
from django.views.generic.base import RedirectView
from django.conf.urls import url, include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/login-perusahaan/', permanent="true"), name='go-to-loginPerusahaan'),
    url(r'^profile-perusahaan/', include(profilPerusahaan, namespace='profile-perusahaan')),
    url(r'^login-perusahaan/', include(loginPerusahaan, namespace='login-perusahaan')),
    url(r'^tanggapanLowongan/', include(tanggapanLowongan,namespace='tanggapan-Lowongan')),
    url(r'^forum-lowongan/', include(forumLowongan, namespace='forum-lowongan')),
]

