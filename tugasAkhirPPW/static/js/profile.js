function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
    IN.Event.on(IN, "auth", getCompanyProfileData);
    IN.Event.on(IN, "auth", checkAdmin);
}
function checkAdmin(){
  var cpnyID = 11397931;
  IN.API.Raw("/companies/" + cpnyID + "/relation-to-viewer/is-company-share-enabled?format=json").method("GET").result(displayAdmin).error(onError);


}
function displayAdmin(data){
  console.log("admin" + data)

}


// Use the API call wrapper to request the member's profile data
function getProfileData() {
IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}
function getCompanyProfileData() {
    var cpnyID = 13602360;
    IN.API.Raw("/companies/" + cpnyID + ":(id,name,website-url,description,company-type,founded-year,specialties,logo-url)?format=json").method("GET").result(displayCompanyProfileData).error(onError);

}

//Handle the successful return from the API call
function displayProfileData(data){
    var user = data.values[0];
    console.log(data);


}
function displayCompanyProfileData(data){
    console.log(data);
    document.getElementById("companyName").innerHTML = data.name;
    document.getElementById("description").innerHTML = data.description;
    document.getElementById("companyType").innerHTML = data.companyType.name;
    document.getElementById("specialties").innerHTML = data.specialties.values;
    document.getElementById("picture").innerHTML =  '<img src="' + data.logoUrl+ '" alt="theFace"">';
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout();
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}
